package com.example.demo;

import java.util.List;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.service.MessageService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { ApplicationConfig.class })
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DemoApplicationTests {

	@Inject
	private MessageService service;

	@Qualifier("dbPrinter")
	@Inject
	private PrinterService prService;

	@Test
	public void testDbPrinter() {

		service.persisteMessage("Hello fucking HSQLfffffffffffffffffffffffffffffffffffffffffffffffffffffffffff!");

		List<Message> messages = service.getNumberOfMessages(100);

		Assert.assertEquals(3, messages.size());
	}

}
