package com.example.demo.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.demo.Message;

public class MessageRowMapper implements RowMapper<Message> {

	@Override
	public Message mapRow(ResultSet rs, int arg1) throws SQLException {

		Message m = new Message();
		m.setId(rs.getInt("id"));
		m.setMessage(rs.getString("message"));
		return m;
	}

}
