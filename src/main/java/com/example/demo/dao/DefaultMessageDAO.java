package com.example.demo.dao;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.Message;

@Repository
@Transactional
public class DefaultMessageDAO extends JdbcDaoSupport implements MessageDAO {

	@Inject
	private DataSource ds;
	private RowMapper<Message> rowMapper = new MessageRowMapper();

	@PostConstruct
	public void init() {
		setDataSource(ds);
		
	}

	@Override
	public List<Message> getMessages() {
		return getJdbcTemplate().query("select * from messages", rowMapper);
	}

	@Override
	public Message getMessageById(Integer id) {
		return getJdbcTemplate().queryForObject("select * from messages where id = ?", new Object[] { id }, rowMapper);
	}

	@Override
	public void addMessage(String message) {
		getJdbcTemplate().update("insert into messages(message) values(?)", message);
	}

	@Override
	public void modifyMessageById(Integer id, String newMessage) {

		Message m = getMessageById(id);

		if (m == null) {
			throw new IllegalArgumentException("Entity not found: " + id);
		}

		getJdbcTemplate().update("update messages set message = ? where id = ? ", newMessage, id);

	}

	@Override
	public void deleteMessageById(Integer id) {

		getJdbcTemplate().update("delete from messages where id = ?", id);

	}

}
