package com.example.demo.dao;

import java.util.List;

import com.example.demo.Message;

public interface MessageDAO {

	List<Message> getMessages();
	
	Message getMessageById(Integer id);
	
	void addMessage(String message);
	
	void modifyMessageById(Integer id, String newMessage);
	
	void deleteMessageById(Integer id);
}
