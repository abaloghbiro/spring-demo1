package com.example.demo.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MessageServiceLoggerAspect {

	private Logger LOGGER = LoggerFactory.getLogger(MessageServiceLoggerAspect.class);

	@Pointcut("execution(* com.example.demo.service.DefaultMessageService.*(String))")
	public void methodCallsInDefaultMessageService() {
	}

	@AfterReturning(value = "methodCallsInDefaultMessageService()")
	public void afterServiceMethodCalled(JoinPoint jp) throws Throwable {

		if ("persisteMessage".equalsIgnoreCase(jp.getSignature().getName())) {
			LOGGER.info("Message which received is fine: " + jp.getArgs()[0]);
		}
	}

	@AfterThrowing(pointcut = "methodCallsInDefaultMessageService()", throwing = "ex")
	public void afterServiceThrowingExceptionMethodCalled(JoinPoint jp, Exception ex) {
		System.out.println("Error while called service method with name: " + jp.getSignature() + " exception: " + ex);

		if ("persisteMessage".equalsIgnoreCase(jp.getSignature().getName())) {
			LOGGER.warn("Message contains the f****ng word, bad guy!");
		}
	}

}
