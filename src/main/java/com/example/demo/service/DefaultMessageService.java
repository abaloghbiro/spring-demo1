package com.example.demo.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.example.demo.Message;
import com.example.demo.dao.MessageDAO;

@Service
public class DefaultMessageService implements MessageService {

	@Inject
	private MessageDAO dao;

	@Override
	public void validateMessageContent(String message) {

		if (message.contains("fucking")) {

			throw new IllegalArgumentException("This is a very ugly message content!!");
		}
	}

	@Override
	public void persisteMessage(String message) {
		validateMessageContent(message);
		dao.addMessage(message);

	}

	@Override
	public List<Message> getNumberOfMessages(Integer boundary) {

		List<Message> o = dao.getMessages();

		return o.stream().filter(m -> m.getId() < boundary).collect(Collectors.toList());
	}

}
