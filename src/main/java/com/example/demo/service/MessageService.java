package com.example.demo.service;

import java.util.List;

import com.example.demo.Message;

public interface MessageService {

	
	void validateMessageContent(String message);
	
	void persisteMessage(String message);
	
	List<Message> getNumberOfMessages(Integer boundary);
}
