package com.example.demo;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.MessageDAO;

@Component
@Qualifier("dbPrinter")
public class DatabasePrinter implements PrinterService {

	@Inject
	private MessageDAO dao;

	@Override
	public void printMessage(String message) {
		System.out.println("Message received: " + message);
		dao.addMessage(message);
	}

}
