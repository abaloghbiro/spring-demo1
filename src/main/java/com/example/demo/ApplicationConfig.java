package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource(locations = {"spring-config/application-context.xml"})
@EnableAspectJAutoProxy
public class ApplicationConfig {

	
	/**
	 * == <bean id="service" class="com.example.demo.ConsolePrinterService"/>

	 * @return
	 */
	@Bean
	public PrinterService getFilePrinterService() {
		return new FilePrinterService();
	}
}
