package com.example.demo;

public interface PrinterService {

	void printMessage(String message);
}
