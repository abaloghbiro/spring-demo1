package com.example.demo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Qualifier;

import com.example.demo.dao.MessageDAO;


@Qualifier("filePrinter")
public class FilePrinterService implements PrinterService {
	
	@Override
	public void printMessage(String message) {

		// create a temp file
		File temp = null;

		// write it
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(temp));
			temp = File.createTempFile("tempfile", ".tmp");
			bw.write(message);
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Temp file created: " + temp.getAbsolutePath());

	}

}
