package com.example.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

	
	public static void main(String[]args) {
		
		ApplicationContext ctx = new AnnotationConfigApplicationContext(ApplicationConfig.class);
		
		ClazzA a = ctx.getBean(ClazzA.class);
		
		System.out.println(a.getService());
	}
}
