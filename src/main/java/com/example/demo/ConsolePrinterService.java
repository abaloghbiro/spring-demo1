package com.example.demo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("consolePrinter")
public class ConsolePrinterService implements PrinterService {

	@Override
	public void printMessage(String message) {
		System.out.println("Message received: " + message);

	}

}
