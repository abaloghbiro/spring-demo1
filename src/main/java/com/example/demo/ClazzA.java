package com.example.demo;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class ClazzA {

	@Qualifier("filePrinter")
	@Inject
	private PrinterService service;

	public PrinterService getService() {
		return service;
	}

}
